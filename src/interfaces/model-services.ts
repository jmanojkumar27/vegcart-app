export interface Query {
    q?: string,
    limit ?: number,
    pageNo ?: number,
    orderBy ?: string,
    soring_order ?: string
  };
  export interface LatLong{
    lat?: number,
    long?: number
  }