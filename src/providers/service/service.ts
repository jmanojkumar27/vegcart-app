
import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { UtilsProvider } from '../utils/utils';

@Injectable()
export class ServiceProvider {
  constructor(private http: Http, private utils: UtilsProvider) {
    // this.setHeader();
  }

  parseQueryCriteria(queryCriteria):any{
    var criterias = [];
    if (queryCriteria.q) {
      criterias.push('q=' + encodeURI(queryCriteria.q));
    }
    if (queryCriteria.limit) {
      criterias.push('limit=' + queryCriteria.limit);
    }
    if (queryCriteria.pageNo) {
      criterias.push('pageNo=' + queryCriteria.pageNo);
    }
    if (queryCriteria.order) {
      criterias.push('order=' + queryCriteria.order);
    }
    if (queryCriteria.orderBy) {
      criterias.push('orderBy=' + queryCriteria.orderBy);
    }
    if (queryCriteria.joinCondition) {
      criterias.push('joinCondition=' + queryCriteria.joinCondition);
    }
    if (queryCriteria.joinConditionList) {
      criterias.push('joinConditionList=' + queryCriteria.joinConditionList);
    }
    return criterias.join('&');
    }
  setHeader() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    if (localStorage.getItem(this.utils.AUTH_TOKEN)) {
      console.log(this.utils.AUTH_TOKEN)
      let token = JSON.parse(localStorage.getItem(this.utils.AUTH_TOKEN));
      console.log(token)
      headers.append('x-access-token',  token.token);
    }

    return new RequestOptions({ headers: headers });
  }

  private logResponse(res: Response) {
    // console.log(res);
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(err: Response | any) {
    return Observable.throw(err.json().message || "Server Error");
  }

  register(jsonReq: Object) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.utils.BASE_URL + "api/authenticate", jsonReq)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  login(jsonReq) {
    return this.http.get(this.utils.BASE_URL+'api/users?'+ this.parseQueryCriteria(jsonReq) ,this.setHeader())
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }
  
  userSignUp(jsonReq: Object) {
    return this.http.post(this.utils.BASE_URL + 'api/signup',jsonReq)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  productList(jsonReq: Object){
    console.log(jsonReq)
    return this.http.get(this.utils.BASE_URL + 'api/productDetails?'+ this.parseQueryCriteria(jsonReq),this.setHeader())
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

}
