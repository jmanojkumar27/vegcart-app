import { Injectable } from '@angular/core';
import { Platform, AlertController, LoadingController, ToastController } from "ionic-angular";

import { SpinnerDialog } from "@ionic-native/spinner-dialog";
import { Toast } from "@ionic-native/toast";
import { UtilsProvider } from '../utils/utils';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class CUtilityProvider {
  private loader;
  safeSvg;
  constructor(public alertCtrl: AlertController, public platform: Platform, public utils: UtilsProvider,
    private loadingCtrl: LoadingController, private toastCtrl: ToastController,
    private spinner: SpinnerDialog, private toast: Toast,private sanitizer: DomSanitizer) {
  }

  /**
   * Is checking device
   */
  isDevice(): boolean {
    if (this.platform.is('android') || this.platform.is('ios') || this.platform.is('windows'))
      return true;
    else return false;
  }

  /**
   * 
   * @param msg 
   * @param btnText 
   * @param btnPositive 
   */
  simpleAlert(msg: string, title?: string, btnText?: string, btnPositive?: any) {
    if (!title)
      title = this.utils.MSG_TITLE;
    if (!btnText)
      btnText = this.utils.MSG_BTNTEXT_POS;
    if (!btnPositive)
      btnPositive = btnPositive;

    this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: [{
        text: btnText,
        handler: btnPositive
      }],
    }).present();
  }

  /**
   * 
   * @param title 
   * @param msg 
   * @param btnText 
   * @param btnPositive 
   * @param btnNegative 
   */
  confirmAlert(msg: string, btnPositive?: any, btnNegative?: any, title?: string, btnText?: Array<string>) {
    if (!title)
      title = this.utils.MSG_TITLE;
    if (!btnText)
      btnText = [this.utils.MSG_BTNTEXT_POS, this.utils.MSG_BTNTEXT_NEG];

    this.alertCtrl.create({
      title: title || this.utils.MSG_TITLE,
      message: msg,
      buttons: [{
        text: btnText[0],
        handler: btnPositive
      }, {
        text: btnText[1],
        handler: btnNegative
      }],
    }).present();
  }

  showSpinner() {
    // try {
      if (this.platform.is('android')) {
    //     this.spinner.show(null, msgContent, true);
    //   }
    //   else {
        // let svg = '<svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-shopping" style="background: none;"><defs><clipPath id="cp" x="0" y="0" width="100" height="100"><rect x="0" y="5" width="100" height="46"/></clipPath></defs><path d="M70,75.2H34.1l-4.1-18.4l-0.7-3l-1-4.7c0,0,0,0,0-0.1c0-0.1,0-0.1-0.1-0.2c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1 c0,0-0.1-0.1-0.1-0.1c0,0-0.1-0.1-0.1-0.1c0,0-0.1-0.1-0.1-0.1c0,0,0,0-0.1-0.1L22.3,44c0-0.1,0-0.2,0-0.3c0-1.9-1.6-3.5-3.5-3.5 s-3.5,1.6-3.5,3.5c0,1.9,1.6,3.5,3.5,3.5c0.7,0,1.4-0.2,2-0.6l4.8,3.7L31.5,77c0,0,0,0,0,0l-5.6,7.7c-0.3,0.5-0.4,1.1-0.1,1.6 c0.3,0.5,0.8,0.8,1.3,0.8h4c-0.8,0.8-1.3,1.9-1.3,3.2c0,2.6,2.1,4.7,4.7,4.7c2.6,0,4.7-2.1,4.7-4.7c0-1.2-0.5-2.3-1.3-3.2h29 c-0.8,0.8-1.3,1.9-1.3,3.2c0,2.6,2.1,4.7,4.7,4.7c2.6,0,4.7-2.1,4.7-4.7c0-1.2-0.5-2.3-1.3-3.2H77c0.8,0,1.5-0.7,1.5-1.5 s-0.7-1.5-1.5-1.5H30l4.3-6h36.8c0.7,0,1.3-0.5,1.4-1.1l7.5-27.3c0.2-0.8-0.2-1.6-1-1.8c-0.8-0.2-1.6,0.2-1.8,1l-1.3,4.7l-0.8,3" ng-attr-fill="{{config.c5}}" fill="#ffc53f"/><polygon points="31.3,53.1 35.7,73.2 68.5,73.2 74,53.1" ng-attr-fill="{{config.c5}}" fill="#ffc53f"/><g clip-path="url(#cp)"><g transform="translate(0 73.1219)"><g transform="translate(50,41)"><path d="M6.5-6.7C6.1-6.9,5.7-7.2,5.3-7.4C5-7.5,4.6-7.7,4.3-7.8C3.1-2.2-4-3.7-2.9-9.3c-0.4,0-0.7,0-1.1,0 c-0.5,0-1,0.1-1.4,0.2c-1.8,0.3-3.6,0.9-5.3,1.8l1.1,4.2l3.1-0.8L-8.7,6.9L3.2,9.3L5.4-1.5l2.5,2l2.7-3.4C9.5-4.4,8.1-5.7,6.5-6.7z" ng-attr-fill="{{config.c1}}" fill="#4658ac" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></path></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed1}}s" repeatCount="indefinite" dur="1.5s"/></g><g transform="translate(0 73.1219)"><g transform="translate(35,17)"><path d="M3.4-5.3L2.5-5l0.8-2.3L1.1-6.3l-1.2-2.2l-1.6,4.6l-4.6-1.6l0.9,2.3l-2.2,1.2l2.3,0.8L-6-0.9 c-0.6,0.3-0.8,0.9-0.5,1.5l1,2.1C-5.2,3.4-4.6,3.6-4,3.3l0.1-0.1l2.1,4.5C-1.4,8.4-0.7,8.7,0,8.3l1.7-0.8l1.7-0.8L5,5.9l1.7-0.8 C7.4,4.8,7.7,4,7.4,3.3L5.2-1.1l0.1-0.1c0.6-0.3,0.8-0.9,0.5-1.5l-1-2.1C4.6-5.4,3.9-5.6,3.4-5.3z" ng-attr-fill="{{config.c2}}" fill="#e7008a" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></path></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed2}}s" repeatCount="indefinite" dur="1.5s"/></g><g transform="translate(0 73.1219)"><g transform="translate(66,26)"><path d="M-4.5-3.7L1.9-6l0.5-0.2L2-7.2l-6.9,2.5C-5.7-4.4-6.1-3.5-6-2.7c0,0.1,0,0.2,0.1,0.3l3,8.2 C-2.5,6.9-1.3,7.4-0.2,7l5.6-2C5.9,4.8,6.2,4.2,6,3.7L3.2-3.9l-0.4-1L2.4-4.7L1.9-4.5l-3.2,1.2l-2.7,1c-0.3,0.1-0.6,0-0.8-0.2 c-0.1-0.1-0.1-0.1-0.1-0.2C-5.1-3.1-4.9-3.6-4.5-3.7z" ng-attr-fill="{{config.c3}}" fill="#ff003a" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></path></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed3}}s" repeatCount="indefinite" dur="1.5s"/></g><g transform="translate(0 73.1219)"><g transform="translate(55,6)"><polygon points="0,-4.9 1.6,-1.7 5.1,-1.1 2.6,1.3 3.2,4.9 0,3.2 -3.2,4.9 -2.6,1.3 -5.1,-1.1 -1.6,-1.7" ng-attr-fill="{{config.c4}}" fill="#ff6d00" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></polygon></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed4}}s" repeatCount="indefinite" dur="1.5s"/></g></g><g clip-path="url(#cp)"><g transform="translate(0,-75)"><g transform="translate(0 73.1219)"><g transform="translate(50,41)"><path d="M6.5-6.7C6.1-6.9,5.7-7.2,5.3-7.4C5-7.5,4.6-7.7,4.3-7.8C3.1-2.2-4-3.7-2.9-9.3c-0.4,0-0.7,0-1.1,0 c-0.5,0-1,0.1-1.4,0.2c-1.8,0.3-3.6,0.9-5.3,1.8l1.1,4.2l3.1-0.8L-8.7,6.9L3.2,9.3L5.4-1.5l2.5,2l2.7-3.4C9.5-4.4,8.1-5.7,6.5-6.7z" ng-attr-fill="{{config.c1}}" fill="#4658ac" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></path></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed1}}s" repeatCount="indefinite" dur="1.5s"/></g><g transform="translate(0 73.1219)"><g transform="translate(35,17)"><path d="M3.4-5.3L2.5-5l0.8-2.3L1.1-6.3l-1.2-2.2l-1.6,4.6l-4.6-1.6l0.9,2.3l-2.2,1.2l2.3,0.8L-6-0.9 c-0.6,0.3-0.8,0.9-0.5,1.5l1,2.1C-5.2,3.4-4.6,3.6-4,3.3l0.1-0.1l2.1,4.5C-1.4,8.4-0.7,8.7,0,8.3l1.7-0.8l1.7-0.8L5,5.9l1.7-0.8 C7.4,4.8,7.7,4,7.4,3.3L5.2-1.1l0.1-0.1c0.6-0.3,0.8-0.9,0.5-1.5l-1-2.1C4.6-5.4,3.9-5.6,3.4-5.3z" ng-attr-fill="{{config.c2}}" fill="#e7008a" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></path></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed2}}s" repeatCount="indefinite" dur="1.5s"/></g><g transform="translate(0 73.1219)"><g transform="translate(66,26)"><path d="M-4.5-3.7L1.9-6l0.5-0.2L2-7.2l-6.9,2.5C-5.7-4.4-6.1-3.5-6-2.7c0,0.1,0,0.2,0.1,0.3l3,8.2 C-2.5,6.9-1.3,7.4-0.2,7l5.6-2C5.9,4.8,6.2,4.2,6,3.7L3.2-3.9l-0.4-1L2.4-4.7L1.9-4.5l-3.2,1.2l-2.7,1c-0.3,0.1-0.6,0-0.8-0.2 c-0.1-0.1-0.1-0.1-0.1-0.2C-5.1-3.1-4.9-3.6-4.5-3.7z" ng-attr-fill="{{config.c3}}" fill="#ff003a" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></path></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed3}}s" repeatCount="indefinite" dur="1.5s"/></g><g transform="translate(0 73.1219)"><g transform="translate(55,6)"><polygon points="0,-4.9 1.6,-1.7 5.1,-1.1 2.6,1.3 3.2,4.9 0,3.2 -3.2,4.9 -2.6,1.3 -5.1,-1.1 -1.6,-1.7" ng-attr-fill="{{config.c4}}" fill="#ff6d00" transform="rotate(341.971)"><animateTransform attributeName="transform" type="rotate" keyTimes="0;1" values="0;360" ng-attr-dur="{{config.rotateSpeed}}s" repeatCount="indefinite" dur="0.75s"/></polygon></g><animateTransform attributeName="transform" type="translate" keyTimes="0;1" values="0 0;0 75" ng-attr-dur="{{config.speed4}}s" repeatCount="indefinite" dur="1.5s"/></g></g></g></svg>';
        let svg = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="lds-vortex" width="85px" height="85px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;">    <g transform="translate(50,50)"><g transform="scale(0.7)"><g transform="translate(-50,-50)"><g transform="rotate(14.7143 50 50)"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" values="360 50 50;0 50 50" keyTimes="0;1" dur="1.6s" keySplines="0.5 0.5 0.5 0.5" calcMode="spline"/><path fill="#3a4938" d="M30.4,9.7c-7.4,10.9-11.8,23.8-12.3,37.9c0.2,1,0.5,1.9,0.7,2.8c1.4-5.2,3.4-10.3,6.2-15.1 c2.6-4.4,5.6-8.4,9-12c0.7-0.7,1.4-1.4,2.1-2.1c7.4-7,16.4-12,26-14.6C51.5,3.6,40.2,4.9,30.4,9.7z"/><path fill="#78ac6c" d="M24.8,64.2c-2.6-4.4-4.5-9.1-5.9-13.8c-0.3-0.9-0.5-1.9-0.7-2.8c-2.4-9.9-2.2-20.2,0.4-29.8 C10.6,25.5,6,36,5.3,46.8C11,58.6,20,68.9,31.9,76.3c0.9,0.3,1.9,0.5,2.8,0.8C31,73.3,27.6,69,24.8,64.2z"/><path fill="#348911" d="M49.6,78.9c-5.1,0-10.1-0.6-14.9-1.8c-1-0.2-1.9-0.5-2.8-0.8c-9.8-2.9-18.5-8.2-25.6-15.2 c2.8,10.8,9.5,20,18.5,26c13.1,0.9,26.6-1.7,38.9-8.3c0.7-0.7,1.4-1.4,2.1-2.1C60.7,78.2,55.3,78.9,49.6,78.9z"/><path fill="#3a4938" d="M81.1,49.6c-1.4,5.2-3.4,10.3-6.2,15.1c-2.6,4.4-5.6,8.4-9,12c-0.7,0.7-1.4,1.4-2.1,2.1 c-7.4,7-16.4,12-26,14.6c10.7,3,22.1,1.7,31.8-3.1c7.4-10.9,11.8-23.8,12.3-37.9C81.6,51.5,81.4,50.6,81.1,49.6z"/><path fill="#78ac6c" d="M75.2,12.9c-13.1-0.9-26.6,1.7-38.9,8.3c-0.7,0.7-1.4,1.4-2.1,2.1c5.2-1.4,10.6-2.2,16.2-2.2 c5.1,0,10.1,0.6,14.9,1.8c1,0.2,1.9,0.5,2.8,0.8c9.8,2.9,18.5,8.2,25.6,15.2C90.9,28.1,84.2,18.9,75.2,12.9z"/><path fill="#348911" d="M94.7,53.2C89,41.4,80,31.1,68.1,23.7c-0.9-0.3-1.9-0.5-2.8-0.8c3.8,3.8,7.2,8.1,10,13 c2.6,4.4,4.5,9.1,5.9,13.8c0.3,0.9,0.5,1.9,0.7,2.8c2.4,9.9,2.2,20.2-0.4,29.8C89.4,74.5,94,64,94.7,53.2z"/></g></g></g></g></svg>';
        this.safeSvg = this.sanitizer.bypassSecurityTrustHtml(svg);
        // sprType = sprType ? sprType : 'bubbles';
        this.loader = this.loadingCtrl.create({
          content: this.safeSvg,
          spinner: 'hide',
        });
        this.loader.present();
      }
    //   }
    // } catch (error) {
    //   console.log(error);
    // }
  }

  hideSpinner() {
    if (this.platform.is('android')) {
      this.loader.dismiss();
    }
  }

  showToast(tMsg: string) {

    console.log(this.platform.is('android'))
    if (this.platform.is('android')) {
      this.toast.show(tMsg, '3000', 'bottom').subscribe(toast => {
        console.log(toast);
      });
    }
    else {
      let toast = this.toastCtrl.create({
        message: tMsg,
        duration: 4000,
        dismissOnPageChange: false,
        position: "middle"
      });
      toast.present();
    }
  }
}
