import { Injectable } from '@angular/core';

/*
  Generated class for the UtilsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UtilsProvider {

  // Base URL of fivestarApp
  BASE_URL: string = "http://ec2-34-227-27-77.compute-1.amazonaws.com:3000/";

  // Common Messages
  MSG_TITLE: string = "FivestarApp";
  MSG_BTNTEXT_POS: string = "YES";
  MSG_BTNTEXT_NEG: string = "NO";
  LOADING_MSG: string = "Please Wait...";

  // localStorage Variables
  REMEMBER: string = "isRemember";
  USERNAME: string = "username";
  AUTH_TOKEN: string = "authToken";
  USER_PROFILE = "userProfile";

  constructor() {
  }

  /**
   * Parsing JSON object
   * @param obj returning JSON string
   */
  jsonToString(jsonObj: Object): string {
    return JSON.stringify(jsonObj);
  }

  /**
   * Parsing JSON object
   * @param obj returning JSON string
   */
  stringToJson(jsonStr: string) {
    return JSON.parse(jsonStr);
  }

  /**
   * Clear localStorage values form the app
   * @param clrName false Clear username and all the storage keys 
   */
  public clearStorage(clrName?: string): void {
    clrName = clrName ? clrName : "true";
    if (clrName == "false") {
      localStorage.removeItem(this.USERNAME);
      localStorage.removeItem(this.REMEMBER);
    }
    localStorage.removeItem(this.AUTH_TOKEN);
    localStorage.removeItem(this.USER_PROFILE);
  }

  async getUserProfile() {
    if (localStorage.getItem(this.USER_PROFILE))
      return await this.stringToJson(localStorage.getItem(this.USER_PROFILE));
  }

}
