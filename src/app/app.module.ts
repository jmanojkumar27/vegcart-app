import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { LoginPage } from '../pages/login/login';


import { MyApp } from './app.component';
import { ScrollableTabs } from '../components/scrollable-tabs';
import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
// import { TabsPage } from '../pages/tabs/tabs';

import { OrderDetailPage } from '../pages/OrderDetailPage/p3';
import { ShoppingCartPage } from '../pages/shopping-cart/shopping-cart';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { ServiceProvider } from '../providers/service/service';
import { UtilsProvider } from '../providers/utils/utils';
import { CUtilityProvider } from '../providers/c-utility/c-utility';
import { HttpModule } from '@angular/http';
import { Toast } from '@ionic-native/toast';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { ProductDescriptionPage } from '../pages/product-description/product-description';
import { UserSignupPage } from '../pages/user-signup/user-signup';
import { ProfilePage } from '../pages/profile/profile';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { MapsPage } from '../pages/maps/maps';
import { CartPage } from '../pages/userCart/p1';
import { MyOrderPage } from '../pages/MyOrderPage/p2';

@NgModule({
  declarations: [
    MyApp,
    ScrollableTabs,
    AboutPage,
    ProfilePage,
    HomePage,
    // TabsPage,
    CartPage,
    MyOrderPage,
    OrderDetailPage,
    LoginPage,
    UserSignupPage,
    MapsPage,
    ShoppingCartPage,
    ProductDescriptionPage
  ],
  imports: [
    BrowserModule,
    SuperTabsModule.forRoot(),
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {}, {
      links: [
        { component: AboutPage, name: 'AboutPage', segment: 'about-page' },
        { component: ProfilePage, name: 'ProfilePage', segment: 'profile-page' },
        { component: HomePage, name: 'HomePage', segment: 'home-page' },
        // { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
        { component: CartPage, name: 'CartPage', segment: 'cart-page' },
        { component: MyOrderPage, name: 'MyOrderPage', segment: 'my-order-page' },
        { component: OrderDetailPage, name: 'OrderDetailPage', segment: 'order-detail-page' },
        { component: LoginPage, name: 'LoginPage', segment: 'login-page' },
        { component: UserSignupPage, name: 'UserSignupPage', segment: 'singup-page' },
        { component: MapsPage, name: 'mapsPage', segment: 'maps' },
        { component: ShoppingCartPage, name: 'ShoppingCartPage', segment: 'shopping-page' },
        { component: ProductDescriptionPage, name: 'ProductDescriptionPage', segment: 'productDetails-page' },
      ]}),
    IonicImageViewerModule,
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ProfilePage,
    HomePage,
    // TabsPage,
    MapsPage,
    CartPage,
    MyOrderPage,
    OrderDetailPage,
    LoginPage,
    UserSignupPage,
    ShoppingCartPage,
    ProductDescriptionPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ServiceProvider,
    UtilsProvider,
    SpinnerDialog,
    Geolocation,
    Toast,
    CUtilityProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
