import { Component , ViewChild} from '@angular/core';
import { Platform , Nav, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
// import { TabsPage } from '../pages/tabs/tabs';
import { UserSignupPage } from '../pages/user-signup/user-signup';

import { MapsPage } from '../pages/maps/maps';
import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage : any;

  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform, 
    statusBar: StatusBar,
    splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // this.secureStorage = new SecureStorage();
     
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      
      statusBar.styleDefault();
      this.loadPage();
      
      // this.rootPage = P1Page;
      splashScreen.hide();
    });
  }
  ionViewDidLoad() {

    this.loadPage();
    // this.rootPage = TabsPage;

  }



  ionViewWillEnter(){
    this.loadPage();
  }
  loadPage(){
    let userLoggedin = localStorage.getItem("userProfile")
    if(userLoggedin)
        this.rootPage = HomePage;
    else
        this.rootPage = LoginPage;
  }
}
