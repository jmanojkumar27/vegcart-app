import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { ShoppingCartPage } from '../shopping-cart/shopping-cart';
import { CUtilityProvider } from '../../providers/c-utility/c-utility';
import { CartPage } from '../userCart/p1';
import { Query } from '../../interfaces/model-services';
import { ServiceProvider } from '../../providers/service/service';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  nav: any;
  public queryforProduct : Query = {};
  public productDetail: any = [];
  public cartproduct : any= [];

  constructor(public app: App,private cutils: CUtilityProvider,public navCtrl: NavController,private service: ServiceProvider) {
    this.ProductList();
    this.productList();
  }


  

  productList(){
    let queryData = [{
      "field": "product_name",
      "op": "equals",
      "value": ""
    }];
    this.queryforProduct.q = JSON.stringify(queryData);
    this.queryforProduct.pageNo = 1;  
    this.queryforProduct.limit = 10;
    this.queryforProduct.orderBy = "created_at";
    this.queryforProduct.soring_order= "asscending";
    this.service.productList(this.queryforProduct).subscribe(
      res => {
        this.users = res.message
        console.log(res)});
  }

  doInfinite(infiniteScroll) {
    let queryData = [{
      "field": "product_name",
      "op": "equals",
      "value": ""
    }];
    this.queryforProduct.q = JSON.stringify(queryData);
    this.queryforProduct.pageNo = this.queryforProduct.pageNo + 1;  
    this.queryforProduct.limit = 10;
    this.queryforProduct.orderBy = "created_at";
    this.queryforProduct.soring_order= "asscending";
    setTimeout(() => {
      this.service.productList(this.queryforProduct).subscribe(
         .subscribe(
           res => {
             this.data = res;
             this.perPage = this.data.per_page;
             this.totalData = this.data.total;
             this.totalPage = this.data.total_pages;
             for(let i=0; i<this.data.data.length; i++) {
               this.users.push(this.data.data[i]);
             }
           },
           error =>  this.errorMessage = <any>error);
  
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

  ProductList() {
    this.productDetail = [{
      "pdt_Id": "01",
      "pdt_Img": "http://www.keeraikadai.in//mobiresize.php?src=http://www.keeraikadai.in/media/keeraikadai/catalog/product/cache/1/small_image/9df78eab33525d08d6e5fb8d27136e95/4/0/40036724_3-fresho-banana-stem-organically-grown.jpg&h=230&zc=1",
      "pdt_Description": "",
      "pdt_Name": "Vazha Thandu",
      "pdt_Price": "₹15",
      "pdt_Quantity": 1
    }, {
      "pdt_Id": "02",
      "pdt_Img": "http://www.keeraikadai.in//mobiresize.php?src=http://www.keeraikadai.in/media/keeraikadai/catalog/product/cache/1/small_image/9df78eab33525d08d6e5fb8d27136e95/m/u/murunga.jpg&h=230&zc=1",
      "pdt_Description": "",
      "pdt_Name": "Murunga keerai",
      "pdt_Price": "₹30",
      "pdt_Quantity": 1
    }, {
      "pdt_Id": "07",
      "pdt_Img": "http://www.keeraikadai.in//mobiresize.php?src=http://www.keeraikadai.in/media/keeraikadai/catalog/product/cache/1/small_image/9df78eab33525d08d6e5fb8d27136e95/2/0/20-pirandai.png&h=230&zc=1",
      "pdt_Description": "",
      "pdt_Name": "Perandai",
      "pdt_Price": "₹20",
      "pdt_Quantity": 1
    }, {
      "pdt_Id": "06",
      "pdt_Img": "http://www.keeraikadai.in//mobiresize.php?src=http://www.keeraikadai.in/media/keeraikadai/catalog/product/cache/1/small_image/9df78eab33525d08d6e5fb8d27136e95/v/a/vallarai.jpg&h=230&zc=1",
      "pdt_Description": "",
      "pdt_Name": "Vallarai keerai",
      "pdt_Price": "₹25",
      "pdt_Quantity": 1
    }, {
      "pdt_Id": "04",
      "pdt_Img": "http://www.keeraikadai.in//mobiresize.php?src=http://www.keeraikadai.in/media/keeraikadai/catalog/product/cache/1/small_image/9df78eab33525d08d6e5fb8d27136e95/n/o/normalmint.jpg&h=230&zc=1",
      "pdt_Description": "",
      "pdt_Name": "Puthina",
      "pdt_Price": "₹25",
      "pdt_Quantity": 1
    }, {
      "pdt_Id": "05",
      "pdt_Img": "http://www.keeraikadai.in//mobiresize.php?src=http://www.keeraikadai.in/media/keeraikadai/catalog/product/cache/1/small_image/9df78eab33525d08d6e5fb8d27136e95/n/o/normalmint.jpg&h=230&zc=1",
      "pdt_Description": "",
      "pdt_Name": "Vazha Poo",
      "pdt_Price": "₹24",
      "pdt_Quantity": 1
    }
    ];
  }
  openProductDetailsPage(data) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.app.getRootNav().push(ShoppingCartPage,{ProductDetail:data});
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.ProductList();

    // set val to the value of the ev target
    var val = ev.target.value;
    console.log(val)

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.productDetail = this.productDetail.filter((item) => {
        return (item.pdt_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  openCartPage(){
    this.app.getRootNav().push(CartPage);
  }
  
  addToCart(product) {
    let isproductAvailable = null;
    this.cartproduct = JSON.parse(localStorage.getItem("cartProduct")) ? JSON.parse(localStorage.getItem("cartProduct")) : [];
    isproductAvailable = this.cartproduct != '' ? this.cartproduct : '';
    if(isproductAvailable)
        isproductAvailable = this.cartproduct.find(prd => prd.pdt_Id === product.pdt_Id);
    if(isproductAvailable)
      this.cutils.showToast("In your cart already have it");
    else {
      this.cartproduct.push(product);
      localStorage.setItem('cartProduct',JSON.stringify(this.cartproduct))
      this.cutils.showToast("successfully added to cart");
    }
  }
  
  ionViewDidLoad() {   
    
  }

}
