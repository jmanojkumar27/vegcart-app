import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import _ from 'underscore';
import { CUtilityProvider } from '../../providers/c-utility/c-utility';
/**
 * Generated class for the P1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-p1',
  templateUrl: 'p1.html',
})
export class CartPage {
  cartItem  : any = [];
  public currentNumber = 0;  
  constructor(public navCtrl: NavController, public alertCtrl: AlertController,public navParams: NavParams,private cutils: CUtilityProvider) {
  }
  public addItem(cartPrd,item){
    this.cartItem.forEach(cartQty => {
      if(cartPrd.pdt_Id == cartQty.pdt_Id){
        if(cartQty.pdt_Quantity == 15){
          this.cutils.showToast("For bulk order please our Team!!!");
        }
        cartQty.pdt_Quantity += 1
      }
    });
    localStorage.setItem('cartProduct',JSON.stringify(this.cartItem))
    this.ionViewDidLoad();
  }
  public removeItem(cartPrd,item){    
    this.cartItem.forEach(cartQty => {
      if(cartPrd.pdt_Id == cartQty.pdt_Id){
        if(cartQty.pdt_Quantity <= 1){
          let confirm = this.alertCtrl.create({
            title: 'Cart',
            message: 'Are you sure want to remove from cart?',
            buttons: [
              {
                text: 'No',
                handler: () => {
                  console.log('Disagree clicked');
                }
              },
              {
                text: 'Yes',
                handler: () => {
                  var filtered = _(this.cartItem).filter(function(pdt) {
                    return pdt.pdt_Id !== cartPrd.pdt_Id
                  });
                  this.cutils.showToast("Item remove from the cart");
                  localStorage.setItem('cartProduct',JSON.stringify(filtered))
                  this.ionViewDidLoad();
                }
              }
            ]
          });
          confirm.present();
        }
        if(cartQty.pdt_Quantity > 1)
            cartQty.pdt_Quantity -= 1
      }
      console.log(this.cartItem)
    });
    localStorage.setItem('cartProduct',JSON.stringify(this.cartItem))
      this.ionViewDidLoad();
  }
  ionViewDidLoad() {   
    this.cartItem = JSON.parse(localStorage.getItem("cartProduct"));
  }
}
