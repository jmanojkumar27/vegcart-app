import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AboutPage } from '../about/about';

import { SuperTabsController, SuperTabs } from 'ionic2-super-tabs';
import { ProfilePage } from '../profile/profile';
import { MyOrderPage } from '../MyOrderPage/p2';
import { CartPage } from '../userCart/p1';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(SuperTabs) superTabs: SuperTabs;
  tab2Root = AboutPage;
  tab3Root = AboutPage;
  tab4Root = AboutPage;
  tab5Root = AboutPage;
  // tab6Root = P3Page;
  constructor(public navCtrl: NavController,private superTabsCtrl: SuperTabsController) {
  }
  
  onTabSelect(tab: { index: number; id: string; }) {
    console.log(`Selected tab: `, tab);
  }

  openUserProfile(){
    this.navCtrl.push(ProfilePage)
  }
  
}
