import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { CUtilityProvider } from '../../providers/c-utility/c-utility';
import _ from 'underscore';

/**
 * Generated class for the P3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-p3',
  templateUrl: 'p3.html',
})
export class OrderDetailPage {
  cartItem  : any = [];
  public currentNumber = 0;  
  constructor(public navCtrl: NavController, public alertCtrl: AlertController,public navParams: NavParams,private cutils: CUtilityProvider) {
  }
  public addItem(cartPrd,item){
    this.cartItem.forEach(cartQty => {
      if(cartPrd.pdt_Id == cartQty.pdt_Id){        
        if(cartQty.pdt_Quantity == 15){
          this.cutils.showToast("For bulk order please our Team!!!");
        }
        cartQty.pdt_Quantity += 1
      }
    });
    localStorage.setItem('cartProduct',JSON.stringify(this.cartItem))
    this.ionViewDidLoad();
  }
  public removeItem(cartPrd,item){    
    this.cartItem.forEach(cartQty => {
      if(cartPrd.pdt_Id == cartQty.pdt_Id){
        if(cartQty.orderedQuantity !== cartQty.pdt_Quantity)
            cartQty.pdt_Quantity -= 1
      }
      console.log(this.cartItem)
    });
    localStorage.setItem('cartProduct',JSON.stringify(this.cartItem))
      this.ionViewDidLoad();
  }
  ionViewDidLoad() {   
    this.cartItem = JSON.parse(localStorage.getItem("cartProduct"));
    this.cartItem.forEach(cartQty => {
      cartQty.orderedQuantity = cartQty.pdt_Quantity
      cartQty.rate = 50;
    })
    console.log(this.cartItem)
  }
}