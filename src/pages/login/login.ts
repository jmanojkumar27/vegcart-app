import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, IonicPageModule } from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CUtilityProvider } from '../../providers/c-utility/c-utility';
import { UtilsProvider } from '../../providers/utils/utils';
import { ServiceProvider } from '../../providers/service/service';
// import { TabsPage } from '../tabs/tabs';
import { UserSignupPage } from '../user-signup/user-signup';

import { Query } from '../../interfaces/model-services';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {
  nav: any;
  loginFrm: FormGroup;
  constructor(public navCtrl: NavController, public _frmBuilder: FormBuilder, private service: ServiceProvider,
    private utils: UtilsProvider, private cutils: CUtilityProvider) {
    this.loginFrm = _frmBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5), Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),Validators.maxLength(25)]],
      password: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      remember: false
    });
  }
  public loginQuery : Query = {};
  ionViewDidLoad() {
    if (localStorage.getItem(this.utils.REMEMBER) === "true") {
      this.loginFrm.patchValue({
        username: localStorage.getItem(this.utils.USERNAME),
        remember: localStorage.getItem(this.utils.REMEMBER)
      })
    }
  }

  forgetPassword() {
    this.navCtrl.push("ForgetPasswordPage");
  }

  onLoginSubmit(formObj) {
    // this.navCtrl.setRoot("HomePage");
    if (!formObj.valid)
    return;
  this.cutils.showSpinner();
  let strQry = {
    "userEmail":formObj.value.username,
    "userPassword":formObj.value.password
    }
  this.service.register(strQry).subscribe(
    res => {
      console.log(res,"res")
        let jdata = {
          userEmail:formObj.value.username,
          userPassword:formObj.value.password
        };
      localStorage.setItem(this.utils.AUTH_TOKEN, this.utils.jsonToString(res));
      console.log(jdata);
      let queryData = [{
        "field": "userEmail",
        "op": "equeals",
        "value": formObj.value.username
      }, {
        "field": "userPassword",
        "op": "equeals",
        "value": formObj.value.password
      }];
      
      this.loginQuery.q = JSON.stringify(queryData); 
      this.service.login(this.loginQuery).subscribe(
        res => {
          console.log(res)
          if (res.message) {
            localStorage.setItem(this.utils.REMEMBER, formObj.value.remember.toString());
            if (formObj.value.remember == true)
              localStorage.setItem(this.utils.USERNAME, formObj.value.username);
            localStorage.setItem(this.utils.USER_PROFILE, this.utils.jsonToString(res.message[0]));
            this.cutils.showToast("Loggedin successfully");
            this.navCtrl.setRoot("HomePage");
            this.cutils.hideSpinner();
          }
          else {
            this.cutils.showToast("Sorry Under Maintenance");
            this.cutils.hideSpinner();
          }
        },
        err => {
          console.log(err);
          this.cutils.showToast(err);
          this.cutils.hideSpinner();
        }
      )
    },
    error => {
      console.log(error);
      this.cutils.showToast(error);
      this.cutils.hideSpinner();
    }
  );

  }
}
