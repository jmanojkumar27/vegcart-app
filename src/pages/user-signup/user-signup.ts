import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { CUtilityProvider } from '../../providers/c-utility/c-utility';
import { UtilsProvider } from '../../providers/utils/utils';
import { ServiceProvider } from '../../providers/service/service';
import { MapsPage } from '../maps/maps';
import { DefaultKeyValueDiffer } from '@angular/core/src/change_detection/differs/default_keyvalue_differ';

import { LatLong } from '../../interfaces/model-services';
/**
 * Generated class for the UserSignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-signup',
  templateUrl: 'user-signup.html',
})
export class UserSignupPage {
  userDetails = {};
  userLocation : LatLong = {};
  userslng; 
  userslat;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private service: ServiceProvider,
    private utils: UtilsProvider, 
    private cutils: CUtilityProvider,
    public modalCtrl: ModalController) {
  }

  openmaps() {
    let mapsModal = this.modalCtrl.create(MapsPage);
    mapsModal.onDidDismiss(data => {
      this.userslat = localStorage.getItem('userLocationLat')
      this.userslng = localStorage.getItem('userLocationLong')
    });
    mapsModal.present();
  }
  userSignup(userDetail){
    if(!userDetail.name){
      this.cutils.showToast("Please enter the username");
      return
    }else if(!userDetail.userAddress){
      this.cutils.showToast("Please enter the address");
      return
    }else if(!userDetail.userEmail){
      this.cutils.showToast("Please enter the email");
      return
    }else if(!userDetail.mobileNumber){
      this.cutils.showToast("Please enter your mobileNumber");
      return
    }else if( !userDetail.userPassword || userDetail.userPassword !== userDetail.pwd){
      this.cutils.showToast("password mismatch");
      return
    }
    let userEntity = {
      "userName": userDetail.name,
      "userPhoneNumber": "+91"+userDetail.mobileNumber,
      "userCountryCode":"+91",
      "userAddress":userDetail.userAddress,
      "userDeliveryAddress":userDetail.userAddress,
      "userEmail" : userDetail.userEmail,
      "userPassword" : userDetail.userPassword
    }
    this.cutils.showSpinner();
    this.service.userSignUp(userEntity).subscribe(
        res => {
          this.cutils.hideSpinner();
          this.cutils.showToast("you have successfully signed up");
          this.navCtrl.setRoot('LoginPage');
        })
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad UserSignupPage');
  }
  goLogin(){
    this.navCtrl.setRoot(LoginPage)
  }

}
