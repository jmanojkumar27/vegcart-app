import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { UtilsProvider } from '../../providers/utils/utils';
import { App } from 'ionic-angular';
import { MyOrderPage } from '../MyOrderPage/p2';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  constructor(public navCtrl: NavController,public appCtrl: App,private utils: UtilsProvider) {

  }

  logout(){
    this.utils.clearStorage("true")
   this.appCtrl.getRootNav().setRoot(LoginPage)
  }
  openUserOrderPage(){
    this.navCtrl.push(MyOrderPage)
  }

}
