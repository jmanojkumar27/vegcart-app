import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController, App} from 'ionic-angular';
import { ImageViewerController } from "ionic-img-viewer";
import { CUtilityProvider } from '../../providers/c-utility/c-utility';
import { LoginPage } from '../login/login';

/**
 * Generated class for the ShoppingCartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
interface itemList {
    "pdt_Id"?:string,
    "pdt_Img"?:string,
    "pdt_Description"?:string,
    "pdt_Name"?:string,
    "pdt_Price"?:string,
    "pdt_Quantity"?:number
}

@IonicPage()
@Component({
  selector: 'page-shopping-cart',
  templateUrl: 'shopping-cart.html',
})
export class ShoppingCartPage {
 public currentNumber = 0;  
 productDetail : any;
 itemList : itemList ={};
  constructor(public app: App,public navCtrl: NavController, public navParams: NavParams,private cutils: CUtilityProvider,public alertCtrl: AlertController,public imageViewerCtrl: ImageViewerController) {
      
  }

  ionViewDidLoad() {
    this.productDetail = this.navParams.get('ProductDetail')
    this.itemList.pdt_Name = this.productDetail.pdt_Name;
    this.itemList.pdt_Description = this.productDetail.pdt_Description;
    this.itemList.pdt_Id = this.productDetail.pdt_Id;
    this.itemList.pdt_Img = this.productDetail.pdt_Img;
    this.itemList.pdt_Quantity = this.productDetail.pdt_Quantity;

  }
  onClick(imageToView) {
    const viewer = this.imageViewerCtrl.create(imageToView)
    viewer.present();
  }
  public addItem(item){
    (item == 15) ? this.currentNumber = 15 : this.currentNumber++;
  }
  public removeItem(item){
    (item == 0) ? this.currentNumber = 0 : this.currentNumber--;
  }
  doPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'BulkOrder',
      message: "Enter a Nos for bulk order",
      inputs: [
        {
          name: 'Nos',
          placeholder: 'ex: 10',
          type:'number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            if(!data.Nos){
              this.cutils.showToast("Enter the nos for bulk order");
            }else{
              this.cutils.showToast("Soonly our team will contact you");
            }
            console.log(data);
            this.app.getRootNav().push(LoginPage)
          }
        }
      ]
    });
    prompt.present();
  }
}
